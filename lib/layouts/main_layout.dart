import 'package:flutter/material.dart';
import 'package:flutter_redux/flutter_redux.dart';
import 'package:redux_practice_5/pages/first_page.dart';
import 'package:redux_practice_5/pages/second_page.dart';
import 'package:redux_practice_5/pages/third_page.dart';
import 'package:redux_practice_5/pages/value_vm.dart';
import 'package:redux_practice_5/store/app/app_state.dart';

class MainLayout extends StatelessWidget {
  final Widget body;
  const MainLayout({Key key, this.body}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return StoreConnector<AppState, ValueVM>(
      converter: (store) => ValueVM.fromStore(store),
      builder: (context, vm) => Scaffold(
        appBar: AppBar(
          title: Text('Navigate'),
          actions: [
            TextButton(
                onPressed: () => Navigator.pushReplacement(
                      context,
                      MaterialPageRoute(
                        builder: (context) => FirstPage(),
                      ),
                    ),
                child: Text('First', style: TextStyle(color: Colors.white))),
            TextButton(
                onPressed: () => Navigator.pushReplacement(
                      context,
                      MaterialPageRoute(
                        builder: (context) => SecondPage(),
                      ),
                    ),
                child: Text('Second', style: TextStyle(color: Colors.white))),
            TextButton(
                onPressed: () => Navigator.pushReplacement(
                      context,
                      MaterialPageRoute(
                        builder: (context) => ThirdPage(),
                      ),
                    ),
                child: Text(
                  'Third',
                  style: TextStyle(color: Colors.white),
                )),
          ],
        ),
        drawer: Drawer(
          child: Column(
            mainAxisAlignment: MainAxisAlignment.center,
            children: [
              IconButton(
                  icon: Icon(Icons.add),
                  iconSize: 40,
                  onPressed: () => vm.increment()),
              IconButton(
                  icon: Icon(Icons.remove),
                  iconSize: 40,
                  onPressed: () => vm.decrement()),
            ],
          ),
        ),
        body: body,
      ),
    );
  }
}
