import 'package:redux/redux.dart';
import 'package:redux_practice_5/store/app/app_state.dart';
import 'package:redux_practice_5/store/shared/value_state/actions/decrement_value.dart';
import 'package:redux_practice_5/store/shared/value_state/actions/increment_value.dart';

class ValueSelector {
  ValueSelector._();
  static void Function() incrementAction(Store<AppState> store) {
    return () => store.dispatch(IncrementValue());
  }

  static void Function() decrementAction(Store<AppState> store) {
    return () => store.dispatch(DecrementValue());
  }

  static int getValue(Store<AppState> store) {
    return store.state.valueState.value;
  }
}
