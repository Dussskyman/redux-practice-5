import 'dart:collection';

import 'package:redux_practice_5/store/shared/reducer.dart';
import 'package:redux_practice_5/store/shared/value_state/actions/decrement_value.dart';
import 'package:redux_practice_5/store/shared/value_state/actions/increment_value.dart';

class ValueState {
  int value;

  ValueState({this.value});

  factory ValueState.initial() {
    return ValueState(value: 0);
  }

  ValueState copyWith({int val}) => ValueState(
        value: val ?? this.value,
      );

  ValueState reducer(dynamic action) {
    return Reducer<ValueState>(
        actions: HashMap.from({
      IncrementValue: (action) => increment(),
      DecrementValue: (action) => decrement(),
    })).updateState(action, this);
  }

  ValueState increment() {
    value++;
    return copyWith(val: value);
  }

  ValueState decrement() {
    value--;
    return copyWith(val: value);
  }
}
