import 'package:flutter/cupertino.dart';
import 'package:redux_practice_5/store/shared/value_state/value_state.dart';

class AppState {
  final ValueState valueState;
  AppState({@required this.valueState});

  factory AppState.initial() {
    return AppState(valueState: ValueState.initial());
  }

  static AppState getReducer(AppState state, dynamic action) {
    return AppState(
      valueState: state.valueState.reducer(action),
    );
  }
}
