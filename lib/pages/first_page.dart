import 'package:flutter/material.dart';
import 'package:flutter_redux/flutter_redux.dart';
import 'package:redux_practice_5/layouts/main_layout.dart';
import 'package:redux_practice_5/pages/value_vm.dart';
import 'package:redux_practice_5/store/app/app_state.dart';

class FirstPage extends StatelessWidget {
  @override
  @override
  Widget build(BuildContext context) {
    return StoreConnector<AppState, ValueVM>(
      converter: (store) => ValueVM.fromStore(store),
      builder: (context, vm) => MainLayout(
        body: Center(
          child: Text(
            'True value \n${vm.value}',
            textAlign: TextAlign.center,
            style: TextStyle(fontSize: 24),
          ),
        ),
      ),
    );
  }
}
