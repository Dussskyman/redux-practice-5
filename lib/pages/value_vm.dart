import 'package:redux/redux.dart';
import 'package:redux_practice_5/store/app/app_state.dart';
import 'package:redux_practice_5/store/shared/value_state/value_selector.dart';

class ValueVM {
  final void Function() increment;
  final void Function() decrement;
  final int value;

  ValueVM({this.increment, this.decrement, this.value});

  static ValueVM fromStore(Store<AppState> store) {
    return ValueVM(
      increment: ValueSelector.incrementAction(store),
      decrement: ValueSelector.decrementAction(store),
      value: ValueSelector.getValue(store),
    );
  }
}
