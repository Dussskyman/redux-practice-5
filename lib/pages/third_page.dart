import 'package:flutter/material.dart';
import 'package:flutter_redux/flutter_redux.dart';
import 'package:redux_practice_5/layouts/main_layout.dart';
import 'package:redux_practice_5/pages/value_vm.dart';
import 'package:redux_practice_5/store/app/app_state.dart';

class ThirdPage extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return StoreConnector<AppState, ValueVM>(
      converter: (store) => ValueVM.fromStore(store),
      builder: (context, vm) => MainLayout(
        body: Center(
          child: Text(
            'Value * 10 \n \n${vm.value * 10}',
            textAlign: TextAlign.center,
            style: TextStyle(fontSize: 24),
          ),
        ),
      ),
    );
  }
}
