import 'package:flutter/material.dart';
import 'package:flutter_redux/flutter_redux.dart';
import 'package:redux/redux.dart';
import 'package:redux_practice_5/pages/first_page.dart';
import 'package:redux_practice_5/store/app/app_state.dart';

void main() {
  final Store store = Store<AppState>(
    AppState.getReducer,
    initialState: AppState.initial(),
  );
  runApp(MyApp(
    store: store,
  ));
}

class MyApp extends StatelessWidget {
  final Store store;
  const MyApp({Key key, this.store}) : super(key: key);
  @override
  Widget build(BuildContext context) {
    return StoreProvider<AppState>(
      store: store,
      child: MaterialApp(
        home: FirstPage(),
      ),
    );
  }
}
